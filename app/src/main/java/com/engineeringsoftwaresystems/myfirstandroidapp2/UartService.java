/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.engineeringsoftwaresystems.myfirstandroidapp2;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server
 * hosted on a given Bluetooth LE device.
 */
public class UartService extends Service {
//	private final static String TAG = UartService.class.getSimpleName();
	String TAG = "B=B "+this.getClass();

	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	private String mBluetoothDeviceAddress;
	private BluetoothGatt mBluetoothGatt;
	private int mConnectionState = STATE_DISCONNECTED;

	private static final int STATE_DISCONNECTED = 0;
	private static final int STATE_CONNECTING = 1;
	private static final int STATE_CONNECTED = 2;

	public final static String ACTION_GATT_CONNECTED = "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.nordicsemi.nrfUART.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.nordicsemi.nrfUART.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.nordicsemi.nrfUART.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.nordicsemi.nrfUART.EXTRA_DATA";
	public final static String DEVICE_DOES_NOT_SUPPORT_UART = "com.nordicsemi.nrfUART.DEVICE_DOES_NOT_SUPPORT_UART";

	public static final UUID TX_POWER_UUID = UUID
			.fromString("00001804-0000-1000-8000-00805f9b34fb");
	public static final UUID TX_POWER_LEVEL_UUID = UUID
			.fromString("00002a07-0000-1000-8000-00805f9b34fb");
	public static final UUID CCCD = UUID
			.fromString("00002902-0000-1000-8000-00805f9b34fb");
	public static final UUID FIRMWARE_REVISON_UUID = UUID
			.fromString("00002a26-0000-1000-8000-00805f9b34fb");
	public static final UUID DIS_UUID = UUID
			.fromString("0000180a-0000-1000-8000-00805f9b34fb");
// ray-tech
	public static final UUID RX_SERVICE_UUID = UUID
			.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
	public static final UUID RX_CHAR_UUID = UUID
			.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
	public static final UUID TX_CHAR_UUID = UUID
			.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
// e-way
//	public static final UUID RX_SERVICE_UUID = UUID
//			.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
//	public static final UUID RX_CHAR_UUID = UUID
//			.fromString("0000fff2-0000-1000-8000-00805f9b34fb");
//	public static final UUID TX_CHAR_UUID = UUID
//			.fromString("0000fff1-0000-1000-8000-00805f9b34fb");

	/* program variables */
	private int stage;
	private int next_msg2send;
	private int iter_count;
	private int cur_level;
	private int prog_mode;   /* 0=manual, 1=auto program mode */
	private int c3_cmd;     /* 0=nothing, 1=start, 2=stop, 4=valid resistance step */
	private boolean req_stop; /* added 2016-12-31 */



	/* user settings */
	private int u_mode;     /* mode desired 0:manual, 1:preset program */
	private int u_timeGoal;
	private int u_distanceGoal;
	private int u_calorieGoal;
	private int u_wattGoal;
	private int u_targetHRC;
	private int u_program;
	private int u_user;
	private int u_language;
	private int u_age;
	private int u_sex;
	private int u_weight;
	private int u_height;
	private int u_englishMetric;

    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

	/* values received from console */
	private int c_wattSupportEnabled;   /* from D1 command byte 3 */
	private int c_hrcSupportEnabled;    /* from D1 command byte 4 */
	private int c_maxLevel;             /* from D1 command byte 5 */
	private int c_maxSlope;             /* from D1 command byte 6 */
	private int c_metricEnglish;        /* from D1 command byte 7 */
	private int c_max_programs;         /* from D1 command byte 8 */
	private int c_modelCoding1;         /* from D1 command byte 13 */
	private int c_modelCoding2;         /* from D1 command byte 14 */

	private int c_current_user;         /* from D2 command byte 3 */
	private int c_current_language;     /* from D2 command byte 4 */
	private int c_current_age;          /* from D2 command byte 5 */
	private int c_current_sex;          /* from D2 command byte 6 */
	private int c_current_weightH;      /* from D2 command byte 7 - hi byte */
	private int c_current_weightL;      /* from D2 command byte 8 - lo byte */
	private int c_current_weight;
	private int c_current_heightH;      /* from D2 command byte 9 - hi byte */
	private int c_current_heightL;      /* from D2 command byte 10- lo byte */
	private int c_current_height;
	private int c_current_totalTimeH;   /* from D2 command byte 11 - ?? */
	private int c_current_totalTimeL;   /* from D2 command byte 12 - ?? */
	private int c_current_totalDistanceH;/*from D2 command byte 13 - ?? */
	private int c_current_totalDistanceL;/*from D2 command byte 14 - ?? */
	private int c_current_totalCaloriesH;/*from D2 command byte 15 - ?? */
	private int c_current_totalCaloriesL;/*from D2 command byte 16 - ?? */

	private int c_current_minutes;      /* from D3 command byte 3 */
	private int c_current_seconds;      /* from D3 command byte 4 */
	private int c_current_distance1_0;  /* from D3 command byte 5 : whole miles/km */
	private int c_current_distance0_1;  /* from D3 command byte 6 : tenths of miles/km */
	private int c_current_distance;
	private int c_current_caloriesH;    /* from D3 command byte 7 : calories hi byte */
	private int c_current_caloriesL;    /* from D3 command byte 8 : calories lo byte */
	private int c_current_calories;
	private int c_current_wattsH;       /* from D3 command byte 9 : hi byte */
	private int c_current_wattsL;       /* from D3 command byte 10: lo byte */
	private int c_current_watts;
	private int c_current_heartrate;    /* from D3 command byte 11 */
	private int c_current_speedH;       /* from D3 command byte 12 : hi byte */
	private int c_current_speedL;       /* from D3 command byte 13 : lo byte */
	private int c_current_speed;
	private int c_current_slope;        /* from D3 command byte 14 */
	private int c_current_level;        /* from D3 command byte 15 : 1-max */
	private int c_current_RPMx10_H;     /* from D3 command byte 16 : RPMx10 - hi byte */
	private int c_current_RPMx10_L;     /* from D3 command byte 17 : RPMx10 - lo byte */
	private int c_current_RPMx10;
	private int c_current_status;       /* from D3 command byte 18 bits 0,1 (00:stopped/end, 01:running, 10=pause) .. bit 2:(0=manual, 1=prog, 2=countdown) */

	private int c_current_mode;         /* from D4 command byte 3 (1=program */
	private int c_current_program;      /* from D4 commamd byte 4 */



	private int debugCounter;
	private boolean b_connected;
	private int sendCommand;

	private boolean b_sendenable;
	private int startDelay;

    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

    /* commanded values */

	private int w_resistance_level;
	private boolean w_new_resistance_level_commanded;
	private int w_workout_run_state;  /* 0=not running, 1=running, 2=paused */
	private int w_workout_mode;       /* 0=manual, 1=auto program */



	/**
	 * Implements callback methods for GATT events that the app cares about. For
	 * example, connection change and services discovered.
	 * 
	 */
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
											int newState) {
			String intentAction;

			Log.d(TAG, "onConnectStateChange");

			if (newState == BluetoothProfile.STATE_CONNECTED) {
				intentAction = ACTION_GATT_CONNECTED;
				mConnectionState = STATE_CONNECTED;
				broadcastUpdate(intentAction);

				b_connected = true;
				sendCommand = 0;
				b_sendenable = true;
				startDelay = 3;


				Log.i(TAG, "Connected to GATT server.");
				// Attempts to discover services after successful connection.
				Log.i(TAG, "Attempting to start service discovery:"
						+ mBluetoothGatt.discoverServices());

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				intentAction = ACTION_GATT_DISCONNECTED;
				mConnectionState = STATE_DISCONNECTED;

				b_connected = false;
				b_sendenable = false;



				Log.i(TAG, "Disconnected from GATT server.");
				broadcastUpdate(intentAction);
			}
		}


		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				Log.w(TAG, "mBluetoothGatt = " + mBluetoothGatt);

				broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);

				enableTXNotification();

			} else {
				Log.w(TAG, "onServicesDiscovered received: " + status);
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
										 BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);

				final byte[] bRecv = characteristic.getValue();
				dumpRxBuffer(bRecv, (byte) bRecv.length);
				processRxBuffer(bRecv, (byte) bRecv.length);

			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {


			broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();

		initVars();

		debugCounter = 0;

		startDelay = 0;


		Log.d(TAG, "onCreate");
	}


	private void broadcastUpdate(final String action) {
		final Intent intent = new Intent(action);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	private void broadcastUpdate(final String action,
			final BluetoothGattCharacteristic characteristic) {
		final Intent intent = new Intent(action);

		if (TX_CHAR_UUID.equals(characteristic.getUuid())) {
			intent.putExtra(EXTRA_DATA, characteristic.getValue());
		} else {
		}
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	public class LocalBinder extends Binder {
		UartService getService() {
			return UartService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		close();
		return super.onUnbind(intent);
	}

	private final IBinder mBinder = new LocalBinder();

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 *
	 * @return Return true if the initialization is successful.
	 */
	public boolean initialize() {
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through
		// BluetoothManager.
		Log.d(TAG, "initialize-in");
		Log.d(TAG, TAG);
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}
Log.d(TAG, "initialize-middle");
		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}
Log.d(TAG, "initialize-end");
		return true;
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 *
	 * @param address
	 *            The device address of the destination device.
	 *
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final String address) {
		Log.d(TAG, "connection-in");
		if (mBluetoothAdapter == null || address == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}

		// Previously connected device. Try to reconnect.
		if (mBluetoothDeviceAddress != null
				&& address.equals(mBluetoothDeviceAddress)
				&& mBluetoothGatt != null) {
			Log.d(TAG,
					"Trying to use an existing mBluetoothGatt for connection.");
			if (mBluetoothGatt.connect()) {
				mConnectionState = STATE_CONNECTING;
				return true;
			} else {
				return false;
			}
		}

		final BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(address);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
		Log.d(TAG, "Trying to create a new connection.");
		mBluetoothDeviceAddress = address;
		mConnectionState = STATE_CONNECTING;
		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		mBluetoothGatt.disconnect();
		// mBluetoothGatt.close();
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt == null) {
			return;
		}
		Log.w(TAG, "mBluetoothGatt closed");
		mBluetoothDeviceAddress = null;
		mBluetoothGatt.close();
		mBluetoothGatt = null;
	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 *
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			Log.d(TAG, "Tag:" + TAG);
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 *
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 */
	/*
	 * public void setCharacteristicNotification(BluetoothGattCharacteristic
	 * characteristic, boolean enabled) { if (mBluetoothAdapter == null ||
	 * mBluetoothGatt == null) { Log.w(TAG, "BluetoothAdapter not initialized");
	 * return; } mBluetoothGatt.setCharacteristicNotification(characteristic,
	 * enabled);
	 * 
	 * 
	 * if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
	 * BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
	 * UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
	 * descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
	 * mBluetoothGatt.writeDescriptor(descriptor); } }
	 */

	/**
	 * Enable TXNotification
	 *
	 * @return
	 */
	public void enableTXNotification() {

		if (mBluetoothGatt == null) {
			showMessage("mBluetoothGatt null" + mBluetoothGatt);
			broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
			return;
		}

		BluetoothGattService RxService = mBluetoothGatt
				.getService(RX_SERVICE_UUID);
		if (RxService == null) {
			showMessage("Rx service not found!");
			broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
			return;
		}
		BluetoothGattCharacteristic TxChar = RxService
				.getCharacteristic(TX_CHAR_UUID);
		if (TxChar == null) {
			showMessage("Tx charateristic not found!");
			broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
			return;
		}
		mBluetoothGatt.setCharacteristicNotification(TxChar, true);

		BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
		descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		mBluetoothGatt.writeDescriptor(descriptor);

	}

	public void writeRXCharacteristic(byte[] value) {

		BluetoothGattService RxService = mBluetoothGatt
				.getService(RX_SERVICE_UUID);
		showMessage("mBluetoothGatt null" + mBluetoothGatt);
		if (RxService == null) {
			showMessage("Rx service not found!");
			broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
			return;
		}
		BluetoothGattCharacteristic RxChar = RxService
				.getCharacteristic(RX_CHAR_UUID);
		if (RxChar == null) {
			showMessage("Rx charateristic not found!");
			broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
			return;
		}
		RxChar.setValue(value);
		boolean status = mBluetoothGatt.writeCharacteristic(RxChar);

		Log.d(TAG, "write TXchar - status=" + status);
	}

	private void showMessage(String msg) {
		Log.e(TAG, msg);
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 *
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null)
			return null;

		return mBluetoothGatt.getServices();
	}

	/* Adding all the IO APIs - Chaitanya Dubey*/
	private void sendC1cmd(int engMetric) {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc1, (byte) 0x05,
				(byte) 0x01, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };

		buf[3] = (byte) ((engMetric != 0) ? 1 : 2); /* metric=1, english=2 */
		len = buf[2];
		byte cs = checkoutsum(buf);
		buf[2+len+1] = cs;
		dumpTxBuffer(buf, len);
		this.writeRXCharacteristic(buf);
	} /* sendC1cmd */

	private void sendC2cmd(int rw, int user, int lang,
						   int age, int sex, int weight,
						   int height) {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc2, (byte) 0x0d,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };

		if (rw != 0) {
            /* write */
			buf[3] = 1;
			buf[4] = (byte) user;
			buf[5] = (byte) lang;
			buf[6] = (byte) age;
			buf[7] = (byte) sex;
			//    weight xx.yy
			buf[8] = (byte) (weight >> 8);  // weight whole number xx // weight >> 8;
			buf[9] = (byte) (weight & 0xff);// weight fraction (yy) // weight & 0xff
			//    height xx.yy (maybe - unconfirmed
			buf[10] = (byte) (height >> 8); // height whole number xx // height >> 8;
			buf[11] = (byte) (height & 0xff);//height fraction (yy) // height & 0xff
		}
		else {
			buf[2] = 1; /* new length */
			buf[3] = 0;
		}

		byte cs = checkoutsum(buf);
		len = buf[2];
		buf[2+len+1] = cs;
		dumpTxBuffer(buf, len);
		this.writeRXCharacteristic(buf);
	} /* sendC2cmd */

	private void sendC3cmd(byte keyValue, byte level, byte slope, int watt) {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc3, (byte) 0x07,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };

		buf[3] = keyValue;
		buf[4] = level;
		buf[5] = slope;
		buf[6] = (byte) (watt / 256);
		buf[7] = (byte) (watt & 0xff);

		byte cs = checkoutsum(buf);
		len = buf[2];
		buf[2+len+1] = cs;
		dumpTxBuffer(buf, len);
		this.writeRXCharacteristic(buf);
	} /* sendC3cmd */


	private void sendC4cmd(byte progNumber, byte mode, byte targetTime,
						   int distance, int calories, int watt,
						   byte targetHeart) {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc4, (byte) 0x0d,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		buf[3] = mode;
		buf[4] = targetTime;
		buf[5] = (byte) (distance / 256);
		buf[6] = (byte) (distance & 0xff);
		buf[7] = (byte) (calories / 256);
		buf[8] = (byte) (calories & 0xff);
		buf[9] = (byte) (watt / 256);
		buf[10]= (byte) (watt & 0xff);
		buf[11]= targetHeart;
		buf[12]= progNumber;

		byte cs = checkoutsum(buf);
		len = buf[2];
		buf[2+len+1] = cs;
		dumpTxBuffer(buf, len);
		this.writeRXCharacteristic(buf);
	} /* sendC4cmd */

	private void sendC5cmd(byte rw, byte dataProgram, byte numData,
						   byte progType,
						   byte data1, byte data2, byte data3, byte data4,
						   byte data5, byte data6, byte data7, byte data8,
						   byte data9, byte data10) {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc5, (byte) 0x0d,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };
		if (rw == 0) {
            /* read */
			buf[3] = 0x00;
			buf[4] = dataProgram;
			buf[5] = numData;
			buf[6] = progType;
			len = 0x04;
		}
		else { /* write */
			buf[3] = 1;
			len = 0x0e;
			buf[4] = dataProgram;
			buf[5] = numData;
			buf[6] = progType;
			buf[7] = data1;
			buf[8] = data2;
			buf[9] = data3;
			buf[10]= data4;
			buf[11]= data5;
			buf[12]= data6;
			buf[13]= data7;
			buf[14]= data8;
			buf[15]= data9;
			buf[16]= data10;
		}
		buf[2] = len;
		byte cs = checkoutsum(buf);
		buf[2+len+1] = cs;
		dumpTxBuffer(buf, (byte) 20);
		this.writeRXCharacteristic(buf);
	} /* sendC5cmd */


	private boolean validateChecksum(byte[] buf, byte len, byte expectedChecksum) {
		byte cs;
		boolean rval;
		int i;

		rval = true;
		cs = 0;
		for (i=0; i<len; i++) {
			cs += buf[i];
		}

		rval = cs == expectedChecksum;
		return rval;
	} /* validateChecksum */

	private int validateRxMessage(byte[] buf, byte len) {
		int rval;

		rval = -1;
		if (len >= 4) {
			if ((buf[0] == (byte) 0xf2) && (buf[1] >= (byte) 0xd0) && (buf[1] <= (byte) 0xdf)) {
				switch (buf[1]) {
					case (byte) 0xd0:
						if ((len == 4) && (buf[2] == 0)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) 3, buf[3])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd1:
						if ((len == (0x0c + 4)) && (buf[2] == 0x0c)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) (0x0c+4-1), (byte) buf[0x0c+4-1])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd2:
						if ((len == (0x0c+4)) && (buf[2] == 0x0c)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) (0x0c+4-1), buf[0x0c+4-1])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd3:
						if ((len == (0x10+4)) && (buf[2] == 0x10)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) (0x10+4-1), buf[0x10+4-1])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd4:
						if ((len == (0x02+4)) && (buf[2] == 0x02)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) (0x02+4-1), buf[0x02+4-1])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd5:
						if ((len == (0x0d+4)) && (buf[2] == 0x0d)) {
                            /* validate checksum */
							if (validateChecksum(buf, (byte) (0x0d+4-1), buf[0x0d+4-1])) {
								rval = 1;
							}
						}
						break;
					case (byte) 0xd6:
						break;
					case (byte) 0xd7:
						break;
					case (byte) 0xd8:
						break;
					case (byte) 0xd9:
						break;
					case (byte) 0xda:
						break;
					default:
						break;
				} /* switch */
			} /* if */
		} /* if len >= 4 */
		return rval;
	} /* validateRxMessage */

	private void dumpTxBuffer(byte[] buf, byte len) {
		StringBuilder sb = new StringBuilder();
		int i;
		for (i=0; i<len; i++) {
			sb.append(String.format("%02x ", buf[i]));
		}
		Log.d(TAG+i, sb.toString());
	} /* dumpTxBuffer */

	public void dumpRxBuffer(byte[] buf, byte len) {
		StringBuilder sb = new StringBuilder();
		int i;
		for (i=0; i<len; i++) {
			sb.append(String.format("%02x ", buf[i]));
		}
		Log.d(TAG+i, sb.toString());
	} /* dumpRxBuffer */


	public void processRxBuffer(byte [] buf, byte len) {
		int isMsgValid;
		isMsgValid = validateRxMessage(buf, len);

		//Log.d("prb", "IsMsgValid(:" + len + "):" + isMsgValid);
		if (isMsgValid < 0) Log.d(TAG, "Invalud Message detected");

		if (isMsgValid > 0) {
			Log.d(TAG, String.format("%02x", buf[1]));
			dumpRxBuffer(buf, len);
			switch (buf[1]) {
				case (byte) 0xd0:
					iter_count = 0;
					next_msg2send = 0xc1;
					break;
				case (byte) 0xd1:
					c_wattSupportEnabled = ((buf[3] != 0) ? 1 : 0);
					c_hrcSupportEnabled = ((buf[4] != 0) ? 1 : 0);
					c_maxLevel = (buf[5]);
					c_maxSlope = (buf[6]);
					c_metricEnglish = buf[7];
					c_current_program = buf[8];
					c_modelCoding1 = buf[12];
					c_modelCoding2 = buf[13];
					iter_count = 0;
					next_msg2send = 0xc2;
					break;
				case (byte) 0xd2:
					if (buf[2] == 0xc2) {
						c_current_user = buf[3];
						c_current_language = buf[4];
						c_current_age = buf[5];
						c_current_sex = buf[6];
						c_current_weightH = byteToint(buf[7]);
						c_current_weightL = byteToint(buf[8]);
						c_current_weight = (c_current_weightH * 256) + c_current_weightL;
						c_current_heightH = byteToint(buf[9]);
						c_current_heightL = byteToint(buf[10]);
						c_current_height = (c_current_heightH * 256) + c_current_heightL;
					}
					iter_count++;
					if (iter_count >= 2) {
						if (prog_mode == 0) {
                            /* manual */
							if ((u_distanceGoal == 0) &&
									(u_timeGoal == 0) &&
									(u_calorieGoal == 0)) {
								next_msg2send = 0xc3;
							}
							else {
								next_msg2send = 0xc4;
							}
						} else if (prog_mode == 1) {
							next_msg2send = 0xc4; /* send parameters */
						}
					}
					break;
				case (byte) 0xd3:
					iter_count++;
					c_current_minutes = buf[3];
					c_current_seconds = buf[4];
					c_current_distance1_0 = byteToint(buf[5]);
					c_current_distance0_1 = byteToint(buf[6]);
					c_current_distance = (c_current_distance1_0 * 256) + c_current_distance0_1;
					c_current_caloriesH = byteToint(buf[7]);
					c_current_caloriesL = byteToint(buf[8]);
					c_current_calories = (c_current_caloriesH * 256) + c_current_caloriesL;
					c_current_wattsH = byteToint(buf[9]);
					c_current_wattsL = byteToint(buf[10]);
					c_current_watts = (c_current_wattsH * 256) + c_current_wattsL;
					c_current_heartrate = buf[11];
					c_current_speedH = byteToint(buf[12]);
					c_current_speedL = byteToint(buf[13]);
					c_current_speed = (c_current_speedH * 256) + c_current_speedL;
					c_current_slope = buf[14];
					c_current_level = buf[15];
					c_current_RPMx10_H = byteToint(buf[16]);
					c_current_RPMx10_L = byteToint(buf[17]);
					c_current_RPMx10 = (c_current_RPMx10_H * 256) + c_current_RPMx10_L;
					c_current_status = buf[18];
					Log.d(TAG, "Min:" + c_current_minutes + ":" + c_current_seconds + ",Distx100:" + c_current_distance + ",Cal:" + c_current_calories + ",Watts:" + c_current_watts + ",HR:" + c_current_heartrate + ",Spdx10:" + c_current_speed + ",Slp:" + c_current_slope + ",Lev:" + c_current_level + ",RPM:" + c_current_RPMx10 + ",Stat:" + c_current_status);
					if ((req_stop) && ((c_current_status & 0x03) != 0)) {
						if (c3_cmd == 0) {
							c3_cmd = 2; /* second pause = stop */
						}
					}
					break;
				case (byte) 0xd4:
					c_current_program = buf[3];
					c_current_mode = buf[4];
					next_msg2send = 0xc3;
					break;
				case (byte) 0xd5:
					next_msg2send = 0xc3;
					iter_count = 1000;
					break;
				case (byte) 0xd6:
					break;
				case (byte) 0xd7:
					break;
				case (byte) 0xd8:
					break;
				case (byte) 0xd9:
					break;
				case (byte) 0xda:
					break;
				case (byte) 0xdb:
					break;
				case (byte) 0xdc:
					break;
				case (byte) 0xdd:
					break;
				case (byte) 0xde:
					break;
				case (byte) 0xdf:
					break;
				default:
					break;
			} /* switch buf[1] */
		} /* if isMsgValid > 0 */
	} /* processRxBuffer */



	public static byte bytenumToASCII(byte byt) {
		int num = 0;
		byte ret = 0;
		num = byteToint(byt);
		num += 48;
		ret = intTobyte(num);
		return ret;
	}

	public static byte intTobyte(int data) {
		byte a = 0;
		a = (byte) (0xff & data);
		// a[1] = (byte) ((0xff00 & i) >> 8);
		// a[2] = (byte) ((0xff0000 & i) >> 16);
		// a[3] = (byte) ((0xff000000 & i) >> 24);
		return a;
	}

	public static int byteToint(byte byt) {
		int b = 0xff;
		b = b & (byt);
		return b;
	}

	public static String bytesToString(byte[] b, int length) {
		StringBuffer result = new StringBuffer("");
		for (int i = 0; i < length; i++) {
			result.append((char) (b[i]));
		}

		return new String(result);// result.toString();
	}

	public static byte checkoutsum(byte[] res) {
		byte byt = 0;
		int length = 0, sum = 0;
		length = res[2] + 3;
		for (int i = 0; i < length; i++) {
			sum += res[i];
		}
		byt = (byte) (sum & 0xff);
		return byt;
	}

	private void sendC0cmd() {

		byte len;
		byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc0, (byte) 0x00,
				(byte) 0xb2, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00, (byte) 0x00,
				(byte) 0x00, (byte) 0x00 };

		len = buf[2];
		dumpTxBuffer(buf, len);
		this.writeRXCharacteristic(buf);
	} /* sendC0cmd */



	public int getConsoleMaxLevels() {
		return c_maxLevel;
	} /* getConsoleMaxLevels */

	public boolean isConsoleMetricMode() {
		return (c_metricEnglish != 0);
	} /* isConsoleMetricMode */

	public boolean isConsoleWattSupported() {
		return (c_wattSupportEnabled != 0);
	} /* isConsoleWattSupported */

	public boolean getConsoleProgram() {
		return (c_current_program != 0);
	} /* getConsoleProgram */

	public boolean isConsoleHRCSupport() {
		return (c_hrcSupportEnabled != 0);
	} /* isConsoleHRCSupport */

	public void setDisconnect() {
		stage = -1;
        /* do disconnect here !!!! ???? */
        /* ?????? */
		this.disconnect(); /* does this work??? */
		Log.d(TAG, "mjp-setDisconnect");
	} /* setDisconnect */

	public boolean getIsConnected() {
		return (stage >= 10);
	} /* getIsConnected */

	public boolean getIsDisconnected() {
		return b_connected;
	} /* getIsDisconnected */

	public int getCurrentConsoleWorkoutRPM() {
		return ( c_current_RPMx10);
	} /* getCurrentConsoleWorkoutRPM */

	public int getCurrentConsoleWorkoutSpeed() {
		return c_current_speed;
	} /* getCurrentConsoleWorkoutSpeed */

	public int getCurrentConsoleWorkoutDistance() {
		return ( c_current_distance);
	} /* getCurrentConsoleWorkoutDistance */

	public int getCurrentConsleWorkoutCalories() {
		return (c_current_calories * 100);
	} /* getCurrentConsoleWorkoutCalories */

	public int getCurrentConsoleWorkoutProgram() {
		return c_current_program;
	} /* getCurrentConsoleWorkoutProgram */

	public int getCurrentConsoleWorkoutUser() {
		return c_current_user;
	} /* getCurrentConsoleWorkoutUser */

	public int getCurrentConsoleWorkoutTimeSeconds() {
		return ((c_current_minutes * 60) + c_current_seconds);
	} /* getCurrentConsoleWorkoutTimeSeconds */

	/* return 0:manual, 1:prog */
	public int getCurrentConsoleWorkoutMode() {
		return c_current_mode >> 2;
	} /* getCurrentConsoleWorkoutMode */

	/* return 0:stop, 1:run, 2:pause */
	public int getCurrentConsoleWorkoutStatus() {
		return (c_current_status & 0x03);
	} /* getCurrentConsoleWorkoutStatus */

	public int getCurrentConsoleWorkoutResistanceLevel() {
		return c_current_level;
	} /* getCurrentConsoleWorkoutResistanceLevel */

	public int getCurrentConsoleWorkoutPulse() {
		return c_current_heartrate;
	} /* getCurrentConsoleWorkoutPulse */

	/* ---------------------------------------------------------- */
    /* ---------------------------------------------------------- */
	public void setNewWorkoutResistanceLevel(int newLevel) {
		if (newLevel > c_current_level) {
            /* increase */
			w_new_resistance_level_commanded = true;
			w_resistance_level = newLevel;
		}
		else if (newLevel < c_current_level) {
            /* decrease */
			w_new_resistance_level_commanded = true;
			w_resistance_level = newLevel;
		}
	} /* setNewWorkoutResistanceLevel */

	public void setEnglishMetric(boolean EnglishMetric) {
		if (EnglishMetric) {
			u_englishMetric = 0;
		}
		else {
			u_englishMetric = 1;
		}
	} /* setEnglishMetric */

	public void setUserParameters(int userNumber,
								  boolean userLanguageEnglish,
								  int userAge,
								  boolean userSexMale,
								  int userHeight,
								  int userWeight) {

		if (userNumber > 0) {
			u_user = userNumber;
		}
		else {
			u_user = 1;
		}

		if (userLanguageEnglish) {
			u_language = 0; /* english */
		}
		else {
			u_language = 0; /* english */
		}

		if (userAge > 0) {
			u_age = userAge;
		}
		else {
			u_age = 25; /* default */
		}

		if (userSexMale) {
			u_sex = 1; /* male */
		}
		else {
			u_sex = 0; /* female */
		}

		if (userHeight > 0) {
			u_height = userHeight;
		}
		else {
			u_height = (u_englishMetric == 0) ? 76 : ((int) (76/2.5)); /* english : metric */
		}

		if (userWeight > 0) {
			u_weight = userWeight;
		}
		else {
			u_weight = (u_englishMetric == 0) ? 100 : ((int) (100/2.2)); /* english : metric */
		}
	} /* setUserParameters */

	public void setUserWorkoutParameters(int modeAuto,
										 int workoutProgramNumber,
										 int workoutCalorieGoal,
										 int workoutDistanceGoal,
										 int workoutTimeGoal, /* minutes */
										 int workoutTargetHRC) {

		u_calorieGoal = 0;
		u_wattGoal = 0;
		u_timeGoal = 0;
		u_distanceGoal = 0;

		if (modeAuto != 0) {
			u_mode = 1;
			prog_mode = 1;
			u_program = workoutProgramNumber;
		}
		else {
			u_mode = 0;
			u_program = 0; /* ?? */
			prog_mode = 0;
		}
		if (workoutTimeGoal > 0) {
			u_timeGoal = workoutTimeGoal;
		}
		else if (workoutCalorieGoal > 0) {
			u_calorieGoal = workoutCalorieGoal;
		}
		else if (workoutDistanceGoal > 0) {
			u_distanceGoal = workoutDistanceGoal;
		}
		stage = 8;
	} /* setUserWorkoutParameterModeAuto */

	public boolean setWorkoutStart() {
		stage = 12;
		next_msg2send = 0xc1;
		iter_count = 0;
		req_stop = false;
		c3_cmd = 1;
		return true;
	} /* setWorkoutStart */

	public boolean setWorkoutPause() {
		c3_cmd = 2;
		req_stop = false;
		return true;
	} /* setWorkoutPause */

	public boolean setWorkoutStop() {
		if ((c_current_status & 0x03) == 1) {
			req_stop = true; // we are run mode --- first do a pause -then pause again=stop
		}
		c3_cmd = 2; /* second pause = stop */
		return true;
	} /* setWorkoutStop */

	public boolean setWorkoutResume() {
		c3_cmd = 1;
		req_stop = false;
		return true;
	} /* setWorkoutResume */

	public void setProgram(int newProg) {
		u_calorieGoal = 0;
		u_wattGoal = 0;
		u_timeGoal = 0;
		u_distanceGoal = 0;

		if (newProg > 0) {
			u_mode = 1;
			prog_mode = 1;
			u_program = newProg;
		}
		else {
			u_mode = 0;
			prog_mode = 0;
			u_program = 0;
		}
	} /* setProgram */

	public void setGoalCal(int newGoal) {
		u_calorieGoal = newGoal;
		if (newGoal != 0) next_msg2send = 0xc4;
		stage = 8;
	} /* setGoalCal */

	public void setGoalDist(int newGoal) {
		u_distanceGoal = newGoal;
		if (newGoal != 0) next_msg2send = 0xc4;
		stage = 8;
	} /* setGoalDist */

	public void setGoalTime(int newGoal) {
		u_timeGoal = newGoal;
		if (newGoal != 0) next_msg2send = 0xc4;
		stage = 8;
	} /* setGoalTime */

	private void initVars() {

		Log.d(TAG,"initVars().begin");

		stage = 0;
		iter_count = 0;
		c_current_status = 0;
		req_stop = false;

		u_user = 1;
		u_mode = 1; /* preset program */
		u_language = 0;
		u_age = 50;
		u_sex = 1;
		u_weight = 150;
		u_height = 65;
		u_englishMetric = 0; /* english */
		u_calorieGoal = 0;
		u_distanceGoal = 0; /* x 100 */
		u_timeGoal = 5; /* minutes */
		u_targetHRC = 0;
		u_program = 13;

		cur_level = 0;
		prog_mode = 1; /* auto program */
		c3_cmd = 0;

        /* initialize console status variables */
		c_wattSupportEnabled = 0;
		c_hrcSupportEnabled = 0;
		c_metricEnglish = 0;
		c_maxLevel = 0;
		c_maxSlope = 0;
		c_current_program = 0;
		c_modelCoding1 = 0;
		c_modelCoding2 = 0;
		c_current_user = 0;
		c_current_language = 0;
		c_current_age = 0;
		c_current_sex = 0;
		c_current_weight = 0;
		c_current_weightH = 0;
		c_current_weightL = 0;
		c_current_heightH = 0;
		c_current_heightL = 0;
		c_current_height = 0;

		Log.d(TAG,"initVars().end");


	} /* initVars */


	public void runStageCmd()
	{
		Log.d(TAG, "stage="+stage + ", startdelay:" + startDelay + ", b_sendenable:" + b_sendenable);

		if (!b_sendenable) stage = 0;

		switch (stage) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				stage++;
				break;
			case 11:
				sendC0cmd();
				iter_count = 0;
				break;
			case 12:
				switch (next_msg2send) {
					case 0xc1:
						sendC1cmd(u_englishMetric);
						break;
					case 0xc2:
						if (iter_count == 0) {
                            /* write */
							sendC2cmd(1, u_user, u_language, u_age, u_sex, u_weight, u_height);
						}
						else {
                            /* read */
							sendC2cmd(0, 0, 0, 0, 0, 0, 0);
						}
						break;
					case 0xc3:
						if (iter_count < 5) {
							sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
						}
						else {
							if (prog_mode == 0) {
                                    /* manual mode */
								if (w_new_resistance_level_commanded) {
									cur_level = w_resistance_level;
									w_new_resistance_level_commanded = false;
									sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
									c3_cmd = 0;
								}
								else {
                                        /* no change in resistance level (c3_cmd=0, 1=start, 2=stop */
									sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte)0, 0);
									c3_cmd = 0; /* added 2016-11-04 -- missing from iOS version */
								}
							}
							else {
                                    /* auto prog mode */
								if (w_new_resistance_level_commanded) {
									cur_level = w_resistance_level;
									w_new_resistance_level_commanded = false;
									sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
									c3_cmd = 0;
								} else {
                                        /* no change in resistance level (c3_cmd=0, 1=start, 2=stop */
									sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
									c3_cmd = 0; /* added 2016-11-04 */
								}
							}
						}
						break;
					case 0xc4:
                        /* set prog mode */
						sendC4cmd((byte) u_program, (byte) u_mode, (byte) u_timeGoal,
								(byte) u_distanceGoal, (byte) u_calorieGoal,
								(byte) u_wattGoal, (byte) u_timeGoal);
						break;
					case 0xc5:
                        /* send program */
						sendC5cmd((byte) 1, (byte) 1, (byte) 0x10,
								(byte) 2, (byte) 1, (byte) 2, (byte) 3,
								(byte) 4, (byte) 5, (byte) 6, (byte) 7,
								(byte) 8, (byte) 9, (byte) 10);
						break;
					case 0xc6:
						break;
				} /* switch next_msg2send */
				break;
			case 13:
				break;
		} /* switch */

	}


	public void doButton2() {
		Toast.makeText(this, "start workout...", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "******* Button2");
		setEnglishMetric(true); /* false = metric */
		setUserParameters(1, true, 50, true, 66, 150);
		setUserWorkoutParameters(1, 12, 0, 0, 10, 0);
		setWorkoutStart();
	}


	public void doButton3() {
		Log.d(TAG, "******* Button3");
		if (debugCounter == 0) {

			Toast.makeText(this, "stop workout...", Toast.LENGTH_SHORT).show();

			setWorkoutStop();
			debugCounter++;
		}
		else {
			setDisconnect();
			debugCounter = 0;
		}
	}




}
