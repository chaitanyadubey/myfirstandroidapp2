
package com.engineeringsoftwaresystems.myfirstandroidapp2;

import android.app.IntentService;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 **/

public class DeviceService extends Service
{
    public static String TAG = "B=B DeviceService";

    public static final String ACTION_SELECT_DEVICE = "com.nordicsemi.ACTION_SELECT_DEVICE";

    public static final String DEVICE_DOES_NOT_SUPPORT_BLE = "com.nordicsemi.DEVICE_DOES_NOT_SUPPORT_BLE";


    private BluetoothAdapter mBluetoothAdapter;

    private List<BluetoothDevice> deviceList;
    private Map<String, Integer> devRssiValues;
    private static final long SCAN_PERIOD = 10 * 1000; /* 10 seconds */
    private Handler mHandler;
    private boolean mScanning;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate.start");

        mHandler = new Handler();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            sendBLENotSuppotedBroadcast();
            stopSelf();
            return;
        }

        /**
         * Initialize the Bluetooth adaptor.  For API level 18 and above, get a
         * reference to BluetoothAdapter thru BluetoothManager.
         */
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Check if Bluetooth is supported on the device
        if (mBluetoothAdapter == null) {
            sendBLENotSuppotedBroadcast();
            stopSelf();
            return;
        }

        Log.d(TAG,"BLE is Supported...");

        Log.d(TAG, "onCreate.end");
    }


    public void sendBLENotSuppotedBroadcast()
    {
      Intent intent = new Intent();
      intent.setAction(DEVICE_DOES_NOT_SUPPORT_BLE);
      sendBroadcast(intent);
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG,"onStartCommand.begin");

        String action = intent.getAction();

        Log.d(TAG,"action="+action);


        if(ACTION_SELECT_DEVICE.equals(action))
        {
            populateList();
        }

        Log.d(TAG,"onStartCommand.end");
        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * Initialize device list container
     */
    private void populateList() {
        /* Initialize device list container */
        Log.d(TAG, "populateList");
        deviceList = new ArrayList<BluetoothDevice>();
        // deviceAdaptor = new DeviceAdapter(this, deviceList);
        devRssiValues = new HashMap<String, Integer>();

        // some screen stuff here - do we need?

        scanLeDevice(true);
    } /* populateList */

    /**
     * Scan BLE Device
     * param enable
     */
    private void scanLeDevice(final boolean enable) {
        Log.d(TAG, "scanLeDevice Start");
        if (enable) {

            Log.d(TAG,""+enable);
            // stop scanning after pre-defined scan period
            mHandler.postDelayed(new Runnable() {


                @Override
                public void run() {
                    Log.d(TAG,""+enable);

                    mScanning = false;

                    Log.d(TAG,"scanLeDevice 2 stopLeScan");
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);


                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);

            Log.d(TAG,"scanLeDevice 4 startLeScan");
        }
        else {

            mScanning = false;
            Log.d(TAG,"scanLeDevice 6 stopLeScan");
            mBluetoothAdapter.stopLeScan(mLeScanCallback);

        }

        Log.d(TAG,"scanLeDevice Ends");
    } /* scanLeDevice */

    /**
     * BLE Scan callback
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi,
                                     byte[] scanRecord) {
                    Log.d(TAG, "onLeScan-run1");


                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "onLeScan-run-run");
                                    addDevice(device, rssi);
                                }
                            }).start();
                        }
                    }).start();
                }
            };

    private void addDevice(BluetoothDevice device, int rssi) {
        boolean deviceFound = false;
        Log.d(TAG, "addDevice name=" + device.getName());

        // TODO : Remove the hardcoding of deviceName, this hardcoding is only for testing purpose.
        String deviceName = "MCF-6902"; // device.getName();

        Log.d(TAG, "Remove the hardcoding addDevice name=" + device.getName());



        Log.d(TAG, "addDevice.getAddress:" + device.getAddress());
        for (BluetoothDevice listDev : deviceList) {
            if (listDev.getAddress().equals(device.getAddress())) {
                Log.i(TAG, listDev.getName() + ":" + listDev.getAddress());
                deviceFound = true;
                break;
            }
        }


        devRssiValues.put(device.getAddress(), rssi);
        if (!deviceFound) {
            deviceList.add(device);

            if( deviceName.equals("EW-MW010") ||
                    deviceName.equals("EW-TW-7968") ||
                    deviceName.equals("EW-TM-7962") ||
                    ((deviceName.length() > 7) && (deviceName.substring(0,8).equals("MCF-6902")))) {
                Log.d(TAG, "found fitness device");
                if (true) {
                    Log.d(TAG, "found fitness device-set RESULT_OK");
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    Bundle b = new Bundle();
                    b.putString(BluetoothDevice.EXTRA_DEVICE, device.getAddress());
                    Intent result = new Intent();
                    result.putExtras(b);
                    result.setAction(ACTION_SELECT_DEVICE);
                    //setResult(Activity.RESULT_OK, result);
                    Log.d(TAG,"sendBroadcast");
                    sendBroadcast(result);
                }
            }


        }

    } /* addDevice */


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy.begin");

        mBluetoothAdapter.stopLeScan(mLeScanCallback);

        Log.d(TAG, "onDestroy.end");

    } /* onDestroy */


}





