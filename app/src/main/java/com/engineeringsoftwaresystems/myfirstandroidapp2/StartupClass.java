package com.engineeringsoftwaresystems.myfirstandroidapp2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by michaelpalumbo on 5/27/17.
 */

public class StartupClass {
    private Context localContext;
    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;
    private List<BluetoothDevice> deviceList;
    private Map<String, Integer> devRssiValues;
    private static final long SCAN_PERIOD = 10 * 1000; /* 10 seconds */
    private boolean mScanning;

    String TAG = "B=B "+this.getClass();
    public StartupClass(Context theContext) {
        super();
        localContext = theContext;

        Log.d(TAG, "create-SC");

        mHandler = new Handler();

        if (!localContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            //Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
            Exception r = new RuntimeException("Feature Not available");
            throw new RuntimeException("BLE Not Supported", r);
        }
        else {
//            Exception r = new RuntimeException("Some message");
//            throw new RuntimeException("It worked", r);
        }

        /**
         * Initialize the Bluetooth adaptor.  For API level 18 and above, get a
         * reference to BluetoothAdapter thru BluetoothManager.
         */
        final BluetoothManager bluetoothManager = (BluetoothManager) localContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Check if Bluetooth is supported on the device
        if (mBluetoothAdapter == null) {
            Exception r = new RuntimeException("NO Bluetooth Service");
            Toast.makeText(theContext, "BLE Not supported", Toast.LENGTH_SHORT).show();
            throw new RuntimeException("BLE Not supported");
        }

        Log.d(TAG, "B4 populateList");

        populateList();
    } /* public StartupClass */

    /**
     * Initialize device list container
     */
    private void populateList() {
        /* Initialize device list container */
        Log.d(TAG, "populateList");
        deviceList = new ArrayList<BluetoothDevice>();
        // deviceAdaptor = new DeviceAdapter(this, deviceList);
        devRssiValues = new HashMap<String, Integer>();

        // some screen stuff here - do we need?
        Log.d(TAG, "b4 scanLeDevice");

//        scanLeDevice(true);
    } /* populateList */


    /**
     * Scan BLE Device
     * param enable
     */
    private void scanLeDevice(final boolean enable) {
        Log.d(TAG, "scanLeDevice");
        if (enable) {
            // stop scanning after pre-defined scan period
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
        else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    } /* scanLeDevice */

    public void doScan() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            ...
//            startScan()
        } else {
//            ...
//            startLeScan()
        }
    } /* doScan */

    /**
     * BLE Scan callback
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi,
                                     byte[] scanRecord) {
                    Log.d(TAG, "LeScanCallback");
                    addDevice(device, rssi);

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Log.d("brad", "onLeScan-run-run");
//                                    addDevice(device, rssi);
//                                }
//                            });
//                        }
//                    });
                } /* public void onLeScan */
            }; /* new BluetoothAdaptor.LeScanCallback */

    private void addDevice(BluetoothDevice device, int rssi) {
        boolean deviceFound = false;
        Log.d(TAG, "addDevice:" + device.getName());

        for (BluetoothDevice listDev : deviceList) {
            if (listDev.getAddress().equals(device.getAddress())) {
                Log.i(TAG, listDev.getName() + ":" + listDev.getAddress());
                deviceFound = true;
                break;
            }
        } /* for */

        devRssiValues.put(device.getAddress(), rssi);
        if (!deviceFound) {
            deviceList.add(device);
            Log.d(TAG, device.getName().substring(0, 8));
            if (device.getName().equals("EW-MW010") ||
                    device.getName().equals("EW-TW-7968") ||
                    device.getName().equals("EW-TM-7962") ||
                    ((device.getName().length() > 7) && (device.getName().substring(0,8).equals("MCF-6902")))) {
                Log.d(TAG, "found fitness device");
            } /* if */

            if (true) {
                Log.d(TAG, "found fitness device-set RESULT_OK");
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                Bundle b = new Bundle();
                b.putString(BluetoothDevice.EXTRA_DEVICE, device.getAddress());
                Intent result = new Intent();
                result.putExtras(b);
//                setResult(Activity.RESULT_OK, result);
            }
        } /* if !deviceFound */


    }

} /* public class StartupClass */
