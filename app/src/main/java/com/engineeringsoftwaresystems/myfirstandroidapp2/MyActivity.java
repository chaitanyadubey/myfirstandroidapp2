package com.engineeringsoftwaresystems.myfirstandroidapp2;

//
//  MyActivity.java
//
//  Created by Michael Palumbo on 9/19/16.
//  Copyright © 2016 Michael Palumbo,
//                   Engineering Software Systems All rights reserved.
//
/* ======================================================================== */
/*            Engineering Software Systems Proprietary                      */
/* This document/file contains proprietary information, and except with     */
/* written permission from Engineering Software Systems, such information   */
/* shall not be published or disclosed to others or used for any purpose    */
/* other than as agreed upon in writing.  This information shall not be     */
/* copied or stored in electronic format, in whole or in part, in source,   */
/* object or executable format.                                             */
/*                                                                          */
/* (C) Copyright 2016-2016 Engineering Software Systems,                    */
/*                            All Rights Reserved                           */
/* ======================================================================== */
/* Module : MyActivity.java                                                 */
/* Date:    October 01, 2016                                                */
/* Author:  Michael J. Palumbo                                              */
/* ======================================================================== */

/* Notes: to interact with this class follow the instructions below         */
/* step 1: create this class                                                */
/* step 2: call init();                                                     */
/* step 3: call myInitialize();                                             */
/* step 4: call myScan4BTDevices();                                         */
/* step 5: monitor if connection established -- if not getIsDisconnected    */
/* step 6: monitor if communication established - if getIsConnected         */
/* step 7: set program info as needed such as:                              */
/*          setEnglishMetric(true);                                         */
/*          setUserParameters(1, true, 50, true, 66, 150);                  */
/*          setUserWorkoutParameters(1, 12, 0, 0, 10, 0);                   */
/* step 8: setWorkoutStart();                                               */
/* step 9: to stop program - setWorkoutStop();                              */
/* step10: to disconnect - setDisconnect();                                 */
/* step11: after getIsDisconnected == true then                             */
/*            can go back to step 3 as needed                               */
/* ======================================================================== */


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;

import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import android.util.Log;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;



public class MyActivity extends AppCompatActivity {

    String TAG = "B=B "+this.getClass();

    TextView textElement;

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_DOWNLOAD_PATH = 3;

 //   private BluetoothAdapter mBtAdapter;
    private UartService mUartService = null;
   // private UARTStatusChangeReceiver mUARTStatusChangeReceiver;

    private DeviceReceiver mDeviceReceiver;

    private String deviceAddress;

    private Timer timer;
    private BluetoothDevice mDevice;

    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

    public void myInitialize() {
    //    initVars();
    } /* myInitialize */


    // UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName ClassName, IBinder rawBinder) {
            Log.d(TAG, "mServiceConnection.onServiceConnected()");
            mUartService = ((UartService.LocalBinder) rawBinder).getService();
            if (!mUartService.initialize()) {
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            mUartService = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // removed debug 2016-11-15
        // enable this to show screen display
        setContentView(R.layout.activity_my);

        Log.d(TAG, "onCreate.start");

         textElement = (TextView) findViewById(R.id.this_is_id_name);
         textElement.setText("I Love You");

        initService();  // prepare to catch bluetooth events

    }


    public void init() {
        timer = new Timer();
        timer.schedule(new RunTimerTask(), 0, 300); // was 300
        // timer.schedule(new ProgramTask(), 0, 1000);

    } // init

    public void myScan4BTDevices()
    {
//        Log.d(TAG,"myScan4BTDevices().begin b_connected="+b_connected);
//
//        if (b_connected == false) {
//           //
//        }

     Log.d(TAG,"myScan4BTDevices().end");
    } // myScan4BTDevices */

    // onClick of 'Send / Connect'  button on screen
    public void sendMessage(View view) {

        Log.d(TAG, "sendMessage.begin");
        textElement.setText("");

        init();
        myInitialize();
        myScan4BTDevices();

        Intent newIntent = new Intent();
        newIntent.setClass(this, DeviceService.class);
        newIntent.setAction(DeviceService.ACTION_SELECT_DEVICE);
        startService(newIntent);


        Log.d(TAG, "sendMessage.end");

    }

    public void doButton2(View view) {
        Toast.makeText(mUartService, "start workout...", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "doButton2.start");
        if(mUartService!=null) {

            try {
                mUartService.doButton2();
            } catch (Exception e) {
                Log.d(TAG, "doButton2 exception="+e.getMessage());
                e.printStackTrace();
            }
        }
        Log.d(TAG, "doButton2.end");
    }

    public void doButton3(View view) {
        Log.d(TAG, "doButton3.start");
        if(mUartService!=null) {

            try {
                mUartService.doButton3();
            } catch (Exception e) {
                Log.d(TAG, "doButton3 exception="+e.getMessage());
                e.printStackTrace();
            }
        }
        Log.d(TAG, "doButton3.end");
    }

    @Override
    public void finish() {
        if (timer != null) {
            timer.purge();
            timer.cancel();
            timer = null;
        }
        super.finish();
    }


    // Initializing the services and receivers
    private void initService() {
        Log.d(TAG, "initService().begin");

        Log.d(TAG, "register DeviceReceiver");
        mDeviceReceiver = new DeviceReceiver();
        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction(DeviceService.ACTION_SELECT_DEVICE);
        intentFilter1.addAction(DeviceService.DEVICE_DOES_NOT_SUPPORT_BLE);
        registerReceiver(mDeviceReceiver,intentFilter1);

         Log.d(TAG, "start DeviceService");
         Intent newIntent = new Intent();
         newIntent.setClass(this, DeviceService.class);
         startService(newIntent);


        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);


        Log.d(TAG, "initService.end");
    }

  // Run at 3.33 Hz
    private class RunTimerTask extends TimerTask {
        private int rt_debugCounter = 0;


        @Override
        public void run()
        {
           mUartService.runStageCmd();
        } /* public void run */

    } /* private class RunTimerTask */


/*
Class DeviceReceiver
Description : This class is used to receive broadcast messages from DeviceService
 */
    public class DeviceReceiver extends BroadcastReceiver
    {
        public static final String TAG = "B=B DeviceReceiver";

        public String bundleToString(Bundle extras)
        {
            final Set<String> keySet = extras.keySet();
            StringBuffer sb = new StringBuffer();
            for (final String key: keySet) {
                sb.append('\"');
                sb.append(key);
                sb.append("\"=\"");
                sb.append(extras.get(key));
                sb.append("\", ");
            }
            return sb.toString();

        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Toast.makeText(context, "onReceive ", Toast.LENGTH_SHORT).show();
            Log.d(TAG,"onReceive");

            String action = intent.getAction();

            Log.d(TAG, "action="+action);


            if(DeviceService.ACTION_SELECT_DEVICE.equals(action)) {
                Bundle bundle = intent.getExtras();


                deviceAddress = bundle.getString(BluetoothDevice.EXTRA_DEVICE);
                mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
                mUartService.connect(deviceAddress);
                Log.d(TAG, "connecting to..." + deviceAddress);
                textElement.setText(deviceAddress);
            }
            else
            if(DeviceService.DEVICE_DOES_NOT_SUPPORT_BLE.equals(action)) {

                textElement.setText("BLE not suppoted");
            }

        }

    }//receiver inner class ends

} /* public class MyActivity extends AppCompatActivity */


